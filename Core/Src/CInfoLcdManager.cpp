/*
 * CInfoLcdManager.cpp
 *
 *  Created on: May 25, 2024
 *      Author: kryst
 */

#include "CInfoLcdManager.h"

CInfoLcdManager::CInfoLcdManager() {
    // TODO Auto-generated constructor stub

}

CInfoLcdManager::~CInfoLcdManager() {
    // TODO Auto-generated destructor stub
}

void CInfoLcdManager::showMinSpeed(const uint32_t& _val, const InfoValType& _type )
{
    if( _type == InfoValType::NORMAL_VAL )
    {
        infoStr = INFO_MIN_SPEED_NORMAL;
        infoStr += std::to_string( _val );
    }
    else
    {
        std::string newInfo;
        newInfo = INFO_MIN_SPEED_NEW;
        newInfo += std::to_string( _val );
        infoNewValuesStrList.push_back(newInfo);
    }
}
void CInfoLcdManager::showMaxSpeed(const uint32_t& _val, const InfoValType& _type )
{
    if( _type == InfoValType::NORMAL_VAL )
    {
        infoStr = INFO_MAX_SPEED_NORMAL;
        infoStr += std::to_string( _val );
    }
    else
    {
        std::string newInfo;
        newInfo = INFO_MAX_SPEED_NEW;
        newInfo += std::to_string( _val );
        infoNewValuesStrList.push_back(newInfo);
    }
}
void CInfoLcdManager::showMaxAcc(const uint32_t& _val, const InfoValType& _type )
{
    if( _type == InfoValType::NORMAL_VAL )
    {
        infoStr = INFO_MAX_ACC_NORMAL;
        infoStr += std::to_string( _val );
    }
    else
    {
        std::string newInfo;
        newInfo = INFO_MAX_ACC_NEW;
        newInfo += std::to_string( _val );
        infoNewValuesStrList.push_back(newInfo);
    }
}
void CInfoLcdManager::showCarName(const std::string& _val, const InfoValType& _type )
{
    if( _type == InfoValType::NORMAL_VAL )
    {
        infoStr = INFO_CAR_NAME_NORMAL;
        infoStr += _val;
    }
    else
    {
        std::string newInfo;
        newInfo = INFO_CAR_NAME_NEW;
        newInfo += _val;
        infoNewValuesStrList.push_back(newInfo);
    }
}
std::string CInfoLcdManager::getInfoStr() const
{
    return infoStr;
}

bool CInfoLcdManager::isNewInfoStr() const
{
    return !infoNewValuesStrList.empty();
}
std::string CInfoLcdManager::getNewInfoStr()
{
    auto lastElement = infoNewValuesStrList.back();
    infoNewValuesStrList.pop_back();
    return lastElement;
}
