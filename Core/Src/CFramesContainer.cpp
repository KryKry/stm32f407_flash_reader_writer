/*
 * CFramesContainer.cpp
 *
 *  Created on: May 24, 2024
 *      Author: kryst
 */

#include <CFramesContainer.h>

CFramesContainer::CFramesContainer() {
    // TODO Auto-generated constructor stub

}
////////////////////////////////////////////////////////////////////////////
CFramesContainer::~CFramesContainer() {
    // TODO Auto-generated destructor stub
}
////////////////////////////////////////////////////////////////////////////
void CFramesContainer::init()
{
//    reserveMemoryForFrames();
}

//void CFramesContainer::reserveMemoryForFrames()
//{
//    framesList.reserve( MAX_FRAMES_QUANTITY );
//
//    for(int i = 0; i < MAX_FRAMES_QUANTITY; ++i)
//    {
//        framesList.emplace_back();
//        framesList.back().reserve( MAX_FRAME_LENGTH );
//    }
//}
////////////////////////////////////////////////////////////////////////////
bool CFramesContainer::addFrame( const uint8_t* _data, const uint32_t* _len )
{
    bool isAdded = false;
    mcu_disable_usb_interrupt();

    if( *_len < MAX_FRAME_LENGTH )
    {
        if( framesList.size() < MAX_FRAMES_QUANTITY )
        {
            std::vector< uint8_t > newFrame;
//            std::copy(_data, _data + *_len, newFrame.begin() );
            newFrame.insert(newFrame.end(), _data, _data + *_len);
            framesList.emplace_back( std::move( newFrame) );
            isAdded = true;
        }
    }
    mcu_enable_usb_interrupt();
    return isAdded;
}
////////////////////////////////////////////////////////////////////////////
std::vector< uint8_t > CFramesContainer::getFrameFromFront()
{
    mcu_disable_usb_interrupt();
    std::vector<uint8_t> frameFromFront;
    if( !framesList.empty() )
    {
        frameFromFront = std::move( framesList.front() );
    }
    framesList.pop_front();

    mcu_enable_usb_interrupt();
    return frameFromFront;
}
////////////////////////////////////////////////////////////////////////////
std::vector< uint8_t > CFramesContainer::getFrameFromBack()
{
    mcu_disable_usb_interrupt();
    std::vector<uint8_t> frameFromFront;
    if( !framesList.empty() )
    {
        frameFromFront = std::move(framesList.back());
    }
    framesList.pop_back();

    mcu_enable_usb_interrupt();
    return frameFromFront;
}
////////////////////////////////////////////////////////////////////////////
bool CFramesContainer::isFrameToProcessed() const
{
    return !framesList.empty();
}
