/*
 * CInfoSender.cpp
 *
 *  Created on: May 24, 2024
 *      Author: kryst
 */

#include <CInfoSender.h>

CInfoSender::CInfoSender() {
    // TODO Auto-generated constructor stub

}

CInfoSender::~CInfoSender() {
    // TODO Auto-generated destructor stub
}
////////////////////////////////////////////////////////////////////////////
/**
 *
 * @return
 */
bool CInfoSender::isFrameToSend() const
{
    return !framesToSendList.empty();
}
////////////////////////////////////////////////////////////////////////////
void CInfoSender::sendNewMinSpeedInfo( const uint32_t& _minSpeed )
{
    std::string str = NEW_MIN_SPEED_INFO;
    str += std::to_string( _minSpeed );
    sendFrame( str );
}
////////////////////////////////////////////////////////////////////////////
/**
 * @brief Send info to PC about max speed
 * @param _maxSpeed
 */
void CInfoSender::sendNewMaxSpeedInfo( const uint32_t& _maxSpeed )
{
    std::string str = NEW_MAX_SPEED_INFO;
    str += std::to_string( _maxSpeed );
    sendFrame( str );
}
////////////////////////////////////////////////////////////////////////////
void CInfoSender::sendNewCarNameInfo( const std::string& _carName )
{
    std::string str = NEW_CAR_NAME_INFO;
    str += _carName;
    sendFrame( str );
}
////////////////////////////////////////////////////////////////////////////
void CInfoSender::sendNewMaxAccInfo( const uint32_t& _maxAcc )
{
    std::string str = NEW_MAX_ACC_INFO;
    str += std::to_string( _maxAcc );
    sendFrame( str );
}
////////////////////////////////////////////////////////////////////////////
void CInfoSender::sendFrame( const std::string& _frame )
{
    mcu_usb_send_frame( (uint8_t*)_frame.data(), _frame.length() );
}
////////////////////////////////////////////////////////////////////////////
