/*
 * CFlashCommunicator.cpp
 *
 *  Created on: May 21, 2024
 *      Author: kryst
 */

#include "CFlashCommunicator.h"

////////////////////////////////////////////////////////////////////////////
CFlashCommunicator::CFlashCommunicator() {

}
////////////////////////////////////////////////////////////////////////////
CFlashCommunicator::~CFlashCommunicator() {

}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::readId()
{
	uint8_t manufacturerId = 0;
	uint8_t deviceId = 0;
	uint8_t data = (uint8_t)EOperationalCode::READ_ID_1;
	uint8_t data2 = 0;
	mcu_spi_flash_chip_select_pin_reset();


	mcu_spi_flash_transmit_data( &data, 1);
	mcu_spi_flash_transmit_data( &data2, 1 );
	mcu_spi_flash_transmit_data( &data2, 1 );
	mcu_spi_flash_transmit_data( &data2, 1 );
	mcu_spi_flash_receive_data( &manufacturerId, 1);
	mcu_spi_flash_receive_data( &deviceId, 1);

	mcu_spi_flash_chip_select_pin_set();

	int c = 0;
}
////////////////////////////////////////////////////////////////////////////
std::vector<uint8_t> CFlashCommunicator::readMemory(const uint32_t& _address, const uint8_t& _byteLen)
{
	uint8_t addrByte2Msb, addrByte1, addrByte0Lsb;
	addrByte2Msb = ( _address & 0xff0000 ) >> 16;
	addrByte1  = ( _address & 0x00ff00 ) >> 8;
	addrByte0Lsb = _address;
	uint8_t* readedData;
	std::vector<uint8_t> readedDataVec;

	readedData = new uint8_t[_byteLen];

	uint8_t instruction = (uint8_t)EOperationalCode::READ;
	mcu_spi_flash_chip_select_pin_reset();
	mcu_spi_flash_transmit_data( &instruction, 1);
	mcu_spi_flash_transmit_data( &addrByte2Msb, 1);
	mcu_spi_flash_transmit_data( &addrByte1, 1);
	mcu_spi_flash_transmit_data( &addrByte0Lsb, 1);
	mcu_spi_flash_receive_data(readedData, _byteLen);
	mcu_spi_flash_chip_select_pin_set();

	int c = 0;

	readedDataVec.reserve( _byteLen );
	std::copy(readedData, readedData + _byteLen, std::back_inserter(readedDataVec) );

	delete[] readedData;
	return readedDataVec;
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::writeEnable()
{
//	if( writeEnableLatch == false )
//	{
		uint8_t instruction = (uint8_t)EOperationalCode::WRITE_ENABLE;

		mcu_spi_flash_chip_select_pin_reset();
		mcu_spi_flash_transmit_data(&instruction, 1);
		mcu_spi_flash_chip_select_pin_set();
//	}

	writeEnableLatch = true;
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::writeDisable()
{
//	if( writeEnableLatch == true )
//	{
		uint8_t instruction = (uint8_t)EOperationalCode::WRITE_DISABLE;

		mcu_spi_flash_chip_select_pin_reset();
		mcu_spi_flash_transmit_data(&instruction, 1);
		mcu_spi_flash_chip_select_pin_set();
//	}

	writeEnableLatch = false;
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::chipErase()
{
	uint8_t instruction = (uint8_t)EOperationalCode::CHIP_ERASE_1;
	writeStatusRegister(0x00);
	writeEnable();

	mcu_spi_flash_chip_select_pin_reset();
	mcu_spi_flash_transmit_data(&instruction, 1);
	mcu_spi_flash_chip_select_pin_set();
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::writeByte(uint8_t _byteVal, uint32_t _address)
{
    uint8_t instruction = (uint8_t)EOperationalCode::BYTE_PROGRAM;
    uint8_t addrByte2Msb, addrByte1, addrByte0Lsb;
    addrByte2Msb = ( _address & 0xff0000 ) >> 16;
    addrByte1  = ( _address & 0x00ff00 ) >> 8;
    addrByte0Lsb = _address;
    writeEnable();

    mcu_spi_flash_chip_select_pin_reset();
    mcu_spi_flash_transmit_data(&instruction, 1);
    mcu_spi_flash_transmit_data( &addrByte2Msb, 1);
    mcu_spi_flash_transmit_data( &addrByte1, 1);
    mcu_spi_flash_transmit_data( &addrByte0Lsb, 1);
    mcu_spi_flash_transmit_data( &_byteVal, 1);
    mcu_spi_flash_chip_select_pin_set();
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::readStatusRegister()
{
    uint8_t instruction = (uint8_t)EOperationalCode::READ_STATUS_REGISTER;
    uint8_t statusRegister = 0;
    mcu_spi_flash_chip_select_pin_reset();
    mcu_spi_flash_transmit_data(&instruction, 1);
    mcu_spi_flash_receive_data(&statusRegister, 1);
    mcu_spi_flash_chip_select_pin_set();

    int a = 0;
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::enableWriteStatusRegister()
{
    uint8_t frame = (uint8_t)EOperationalCode::ENABLE_WRITE_STATUS_REGISTER;
    mcu_spi_flash_chip_select_pin_reset();
    mcu_spi_flash_transmit_data( &frame, 1 );
    mcu_spi_flash_chip_select_pin_set();
}
////////////////////////////////////////////////////////////////////////////
void CFlashCommunicator::writeStatusRegister(const uint8_t& _status)
{
    enableWriteStatusRegister();
    uint8_t frame[2] = {  (uint8_t)EOperationalCode::WRITE_STATUS_REGISER,  _status };
    mcu_spi_flash_chip_select_pin_reset();
    mcu_spi_flash_transmit_data( frame, 2 );
    mcu_spi_flash_chip_select_pin_set();
}


