/*
 * Mcu2.h
 *
 *  Created on: May 21, 2024
 *      Author: kryst
 */


#include "Mcu2.h"

////////////////////////////////////////////////////////////////////////////
HAL_StatusTypeDef mcu_spi_flash_transmit_data(uint8_t* _data, size_t _size)
{
	return HAL_SPI_Transmit(&hspi1, _data, _size, SPI_FLASH_WRITE_TIMEOUT);
}
////////////////////////////////////////////////////////////////////////////
HAL_StatusTypeDef mcu_spi_flash_receive_data(uint8_t* _data, size_t _size)
{
	return HAL_SPI_Receive(&hspi1, _data, _size, SPI_FLASH_READ_TIMEOUT);
}
////////////////////////////////////////////////////////////////////////////
void mcu_spi_flash_chip_select_pin_set()
{
	HAL_GPIO_WritePin(SPI1_FLASH_CS_GPIO_Port, SPI1_FLASH_CS_Pin, GPIO_PIN_SET);
}
////////////////////////////////////////////////////////////////////////////
void mcu_spi_flash_chip_select_pin_reset()
{
	HAL_GPIO_WritePin(SPI1_FLASH_CS_GPIO_Port, SPI1_FLASH_CS_Pin, GPIO_PIN_RESET);
	mcu_delay_ms( SPI_FLASH_MIN_TIME_SPACE_MS_BETWEEN_FRAMES );
}
////////////////////////////////////////////////////////////////////////////
void mcu_delay_ms(uint32_t _timeMs)
{
    HAL_Delay( _timeMs );
}
////////////////////////////////////////////////////////////////////////////
void mcu_enable_usb_interrupt()
{
    NVIC_EnableIRQ( OTG_FS_IRQn );
}
////////////////////////////////////////////////////////////////////////////
void mcu_disable_usb_interrupt()
{
    NVIC_DisableIRQ( OTG_FS_IRQn );
}
////////////////////////////////////////////////////////////////////////////
void mcu_usb_send_frame(uint8_t* _data, uint16_t _len)
{
    usb_send_answer(_data, _len);
}

