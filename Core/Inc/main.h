/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CS_I2C_SPI_Pin GPIO_PIN_3
#define CS_I2C_SPI_GPIO_Port GPIOE
#define PC14_OSC32_IN_Pin GPIO_PIN_14
#define PC14_OSC32_IN_GPIO_Port GPIOC
#define PC15_OSC32_OUT_Pin GPIO_PIN_15
#define PC15_OSC32_OUT_GPIO_Port GPIOC
#define PH0_OSC_IN_Pin GPIO_PIN_0
#define PH0_OSC_IN_GPIO_Port GPIOH
#define PH1_OSC_OUT_Pin GPIO_PIN_1
#define PH1_OSC_OUT_GPIO_Port GPIOH
#define OTG_FS_PowerSwitchOn_Pin GPIO_PIN_0
#define OTG_FS_PowerSwitchOn_GPIO_Port GPIOC
#define PDM_OUT_Pin GPIO_PIN_3
#define PDM_OUT_GPIO_Port GPIOC
#define B1_Pin GPIO_PIN_0
#define B1_GPIO_Port GPIOA
#define I2S3_WS_Pin GPIO_PIN_4
#define I2S3_WS_GPIO_Port GPIOA
#define SPI1_FLASH_SCK_Pin GPIO_PIN_5
#define SPI1_FLASH_SCK_GPIO_Port GPIOA
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define DISP_RS_Pin GPIO_PIN_7
#define DISP_RS_GPIO_Port GPIOE
#define DISP_RW_Pin GPIO_PIN_10
#define DISP_RW_GPIO_Port GPIOE
#define DISP_ENA_Pin GPIO_PIN_11
#define DISP_ENA_GPIO_Port GPIOE
#define DISP_DB4_Pin GPIO_PIN_12
#define DISP_DB4_GPIO_Port GPIOE
#define DISP_DB5_Pin GPIO_PIN_13
#define DISP_DB5_GPIO_Port GPIOE
#define DISP_DB6_Pin GPIO_PIN_14
#define DISP_DB6_GPIO_Port GPIOE
#define DISP_DB7_Pin GPIO_PIN_15
#define DISP_DB7_GPIO_Port GPIOE
#define CLK_IN_Pin GPIO_PIN_10
#define CLK_IN_GPIO_Port GPIOB
#define LD4_Pin GPIO_PIN_12
#define LD4_GPIO_Port GPIOD
#define LD3_Pin GPIO_PIN_13
#define LD3_GPIO_Port GPIOD
#define LD5_Pin GPIO_PIN_14
#define LD5_GPIO_Port GPIOD
#define LD6_Pin GPIO_PIN_15
#define LD6_GPIO_Port GPIOD
#define SW_4_UP_Pin GPIO_PIN_6
#define SW_4_UP_GPIO_Port GPIOC
#define SW_4_UP_EXTI_IRQn EXTI9_5_IRQn
#define I2S3_MCK_Pin GPIO_PIN_7
#define I2S3_MCK_GPIO_Port GPIOC
#define SW_4_DOWN_Pin GPIO_PIN_8
#define SW_4_DOWN_GPIO_Port GPIOC
#define SW_4_DOWN_EXTI_IRQn EXTI9_5_IRQn
#define SW_3_LEFT_Pin GPIO_PIN_9
#define SW_3_LEFT_GPIO_Port GPIOC
#define SW_3_LEFT_EXTI_IRQn EXTI9_5_IRQn
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define SW_2_CENTER_Pin GPIO_PIN_15
#define SW_2_CENTER_GPIO_Port GPIOA
#define SW_2_CENTER_EXTI_IRQn EXTI15_10_IRQn
#define I2S3_SCK_Pin GPIO_PIN_10
#define I2S3_SCK_GPIO_Port GPIOC
#define SW_1_RIGHT_Pin GPIO_PIN_11
#define SW_1_RIGHT_GPIO_Port GPIOC
#define SW_1_RIGHT_EXTI_IRQn EXTI15_10_IRQn
#define I2S3_SD_Pin GPIO_PIN_12
#define I2S3_SD_GPIO_Port GPIOC
#define Audio_RST_Pin GPIO_PIN_4
#define Audio_RST_GPIO_Port GPIOD
#define OTG_FS_OverCurrent_Pin GPIO_PIN_5
#define OTG_FS_OverCurrent_GPIO_Port GPIOD
#define SPI1_FLASH_CS_Pin GPIO_PIN_7
#define SPI1_FLASH_CS_GPIO_Port GPIOD
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define SPI1_FLASH_MISO_Pin GPIO_PIN_4
#define SPI1_FLASH_MISO_GPIO_Port GPIOB
#define SPI1_FLASH_MOSI_Pin GPIO_PIN_5
#define SPI1_FLASH_MOSI_GPIO_Port GPIOB
#define Audio_SCL_Pin GPIO_PIN_6
#define Audio_SCL_GPIO_Port GPIOB
#define Audio_SDA_Pin GPIO_PIN_9
#define Audio_SDA_GPIO_Port GPIOB
#define MEMS_INT2_Pin GPIO_PIN_1
#define MEMS_INT2_GPIO_Port GPIOE

/* USER CODE BEGIN Private defines */


int add_frame(uint8_t* _buf, uint32_t* _len);
int usb_send_answer(uint8_t* _data, uint16_t _len);

//volatile uint8_t answer[40] = {0};
//volatile uint16_t answerSize = 0;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
