/*
 * CInfoLcdManager.h
 *
 *  Created on: May 25, 2024
 *      Author: kryst
 */

#ifndef CINFOLCDMANAGER_H_
#define CINFOLCDMANAGER_H_

#include <string>
#include <list>

#define INFO_MIN_SPEED_NORMAL "minSpeed "
#define INFO_MIN_SPEED_NEW "new minSpeed "
#define INFO_MAX_SPEED_NORMAL "maxSpeed "
#define INFO_MAX_SPEED_NEW "new maxSpeed "
#define INFO_MAX_ACC_NORMAL "maxAcc   "
#define INFO_MAX_ACC_NEW "new maxAcc   "
#define INFO_CAR_NAME_NORMAL "car name "
#define INFO_CAR_NAME_NEW "new name "

class CInfoLcdManager {
public:
    enum class InfoValType
    {
        NORMAL_VAL,
        NEW_VAL
    };


    CInfoLcdManager();
    virtual ~CInfoLcdManager();

    void showMinSpeed(const uint32_t& _val, const InfoValType& _type );
    void showMaxSpeed(const uint32_t& _val, const InfoValType& _type );
    void showMaxAcc(const uint32_t& _val, const InfoValType& _type );
    void showCarName(const std::string& _val, const InfoValType& _type );

    std::string getInfoStr() const;
    bool isNewInfoStr() const;
    std::string getNewInfoStr();

private:
    std::list< std::string > infoNewValuesStrList;
    std::string infoStr;
};

#endif /* CINFOLCDMANAGER_H_ */
