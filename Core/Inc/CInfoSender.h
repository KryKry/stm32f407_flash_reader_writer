/*
 * CInfoSender.h
 *
 *  Created on: May 24, 2024
 *      Author: kryst
 */

#ifndef INC_CINFOSENDER_H_
#define INC_CINFOSENDER_H_

#include <list>
#include <vector>
#include <string>

#include "Mcu2.h"

#define NEW_MIN_SPEED_INFO "New min speed: "
#define NEW_MAX_SPEED_INFO "New max speed: "
#define NEW_CAR_NAME_INFO "New car name: "
#define NEW_MAX_ACC_INFO "New max acc: "

class CInfoSender {
public:
    CInfoSender();
    virtual ~CInfoSender();

    bool isFrameToSend() const;
    void sendNewMinSpeedInfo( const uint32_t& _minSpeed );
    void sendNewMaxSpeedInfo( const uint32_t& _minSpeed );
    void sendNewCarNameInfo( const std::string& _carName );
    void sendNewMaxAccInfo( const uint32_t& _maxAcc );

private:
    void sendFrame( const std::string& _frame );

    std::list< std::string > framesToSendList;

};

#endif /* INC_CINFOSENDER_H_ */
