/*
 * CJsonConfigReaderWriter.h
 *
 *  Created on: May 22, 2024
 *      Author: kryst
 */

#ifndef INC_CJSONCONFIGREADERWRITER_H_
#define INC_CJSONCONFIGREADERWRITER_H_

#include "json.hpp"
#include <string>

#include "CFlashCommunicator.h"
#include "CFramesContainer.h"
#include "CInfoSender.h"
#include "CInfoLcdManager.h"

#define END_OF_JSON_MARKER 0x00
#define MAX_SIZE_JSON 100

class CJsonConfigReaderWriter {
public:
    explicit CJsonConfigReaderWriter(CFlashCommunicator* _flashCommunicator, CFramesContainer* _framesContainer, CInfoSender* _infoSender, CInfoLcdManager* _infoLcdManager);
    virtual ~CJsonConfigReaderWriter();

    void createJson();
    bool readJsonFromFlash();
    void processFramesFromPc();

    int getMinSpeed() const;
    int getMaxSpeed() const;
    int getMaxAcc() const;
    std::string getCarName() const;


private:

    bool readJsonFrameFromPC();
    void cutToEndMarker(std::string& _str);
    template <typename T>
    bool updateConfigValue(nlohmann::json& json, const std::string& key, T& currentValue); // , std::function<void(const T&)> sendInfo

    CFlashCommunicator* flashCommunicator;
    CFramesContainer* framesContainer;
    CInfoSender* infoSender;
    CInfoLcdManager* infoLcdManager;

    int minSpeed = 20;
    int maxSpeed = 90;
    int maxAcc = 17.25;
    std::string carName = "CVolvo";

    nlohmann::json json;
};

#endif /* INC_CJSONCONFIGREADERWRITER_H_ */
