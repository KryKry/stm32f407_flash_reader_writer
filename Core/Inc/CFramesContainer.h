/*
 * CFramesContainer.h
 *
 *  Created on: May 24, 2024
 *      Author: kryst
 */

#ifndef INC_CFRAMESCONTAINER_H_
#define INC_CFRAMESCONTAINER_H_

#include "stdint.h"
#include <vector>
#include <list>

#include "Mcu2.h"

#define MAX_FRAMES_QUANTITY 10
#define MAX_FRAME_LENGTH 50

class CFramesContainer {
public:
    CFramesContainer();
    virtual ~CFramesContainer();

    bool isFrameToProcessed() const;
    bool addFrame( const uint8_t* _data, const uint32_t* _len );
    std::vector< uint8_t > getFrameFromFront();
    std::vector< uint8_t > getFrameFromBack();

private:
    void init();
    void reserveMemoryForFrames();
    std::list< std::vector<uint8_t> > framesList;


};

#endif /* INC_CFRAMESCONTAINER_H_ */
