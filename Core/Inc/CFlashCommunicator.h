/*
 * CFlashCommunicator.h
 *
 *  Created on: May 21, 2024
 *      Author: kryst
 */

#ifndef CFLASHCOMMUNICATOR_H_
#define CFLASHCOMMUNICATOR_H_

#include <memory>
#include <vector>

#include "Mcu2.h"

class CFlashCommunicator {

public:
	enum class EOperationalCode : uint8_t
	{
		WRITE_STATUS_REGISER = 0x01,
		BYTE_PROGRAM = 0x02,
		READ = 0x03,
		WRITE_DISABLE = 0x04,
		READ_STATUS_REGISTER = 0x05,
		WRITE_ENABLE = 0x06,
		HIGH_SPEED_READ = 0x0B,
		ERASE_SECTOR_4_KBYTE = 0x20,
		ENABLE_WRITE_STATUS_REGISTER = 0x50,
		ERASE_SECTOR_32_KBYTE = 0x52,
		CHIP_ERASE_1 = 0x60,
		EBSY = 0x70, // enable SO to output RY/BY status during AAI programming
		DBSY = 0x80, // disable SO to output RY.BY status during AAI programming
		READ_ID_1 = 0x90,
		JEDEC_ID_READ = 0x9F,
		READ_ID_2 = 0xAB,
		AUTO_ADDRESS_INCREMENT_PROGRAMMING = 0xAD,
		CHIP_ERASE_2 = 0xC7,
		ERASE_SECTOR_64_KBYTE = 0xD8,
	};

	enum class EMemoryAddress : uint32_t
	{
		LOWEST_ADDRESS = 0x000000,
		MANUFACTURER_ID = 0x000000,
		DEVICE_ID = 0x000001,
		HIGHEST_ADDRESS = 0x1FFFFF
	};

	CFlashCommunicator();
	virtual ~CFlashCommunicator();

	void readId();
	/**
	 *
	 * @param _address
	 * @param _byteLen
	 * @return
	 */
	std::vector<uint8_t> readMemory(const uint32_t& _address, const uint8_t& _byteLen);
	void chipErase();
	void writeByte(uint8_t _byteVal, uint32_t _address);
	void readStatusRegister();
	/**
	 *
	 */
	void enableWriteStatusRegister();
	void writeStatusRegister(const uint8_t& _status);

private:
	void writeEnable();
	void writeDisable();
	void writeAddress(const uint32_t& _address);

	bool writeEnableLatch = false;
};

#endif /* CFLASHCOMMUNICATOR_H_ */
