/*
 * CJsonConfigReaderWriter.cpp
 *
 *  Created on: May 22, 2024
 *      Author: kryst
 */

#include "CJsonConfigReaderWriter.h"

using Json = nlohmann::json;

////////////////////////////////////////////////////////////////////////////
CJsonConfigReaderWriter::CJsonConfigReaderWriter(CFlashCommunicator* _flashCommunicator, CFramesContainer* _framesContainer,
        CInfoSender* _infoSender, CInfoLcdManager* _infoLcdManager)
    : flashCommunicator(_flashCommunicator), framesContainer(_framesContainer), infoSender(_infoSender), infoLcdManager( _infoLcdManager )
{
    // TODO Auto-generated constructor stub

}
////////////////////////////////////////////////////////////////////////////
CJsonConfigReaderWriter::~CJsonConfigReaderWriter() {
    // TODO Auto-generated destructor stub
}
////////////////////////////////////////////////////////////////////////////
void CJsonConfigReaderWriter::createJson()
{
    json["minSpeed"] = minSpeed;
    json["maxSpeed"] = maxSpeed;
    json["maxAcc"] = maxAcc;
    json["carName"] = carName;

    static_assert(std::is_same_v<unsigned char, uint8_t>);
    uint8_t* data = reinterpret_cast<std::uint8_t*>( json.dump().data() );

    flashCommunicator->chipErase();

    mcu_delay_ms(50);

    size_t i = 0;

    for(i = 0; i < json.dump().size(); i++ )
    {
        flashCommunicator->writeByte(data[i], i);
    }

    flashCommunicator->writeByte( END_OF_JSON_MARKER, i);

//    flashCommunicator->writeByte(_byteVal, _address)
}
////////////////////////////////////////////////////////////////////////////
bool CJsonConfigReaderWriter::readJsonFromFlash()
{
    uint32_t address = 0;

    auto vecData = flashCommunicator->readMemory(address, (uint8_t)MAX_SIZE_JSON);

    std::string jsonString( vecData.begin(), vecData.end() );
    cutToEndMarker( jsonString );


    json = Json::parse(jsonString );
    if( !json.is_discarded() )
    {
        if( json.find("minSpeed") != json.end() )
            maxSpeed = json.find("maxSpeed")->get<int>();
        if( json.find("maxSpeed") != json.end() )
            minSpeed = json.find("minSpeed")->get<int>();
        if( json.find("maxAcc") != json.end() )
            maxAcc = json.find("maxAcc")->get<int>();
        if( json.find("carName") != json.end() )
            carName = json.find("carName")->get<std::string>();
        return true;
    }
    else
    {
        return false;
    }



    int d = 0;
}
////////////////////////////////////////////////////////////////////////////
void CJsonConfigReaderWriter::cutToEndMarker(std::string& _str)
{
    size_t pos = _str.find( (char)END_OF_JSON_MARKER );
    if (pos != std::string::npos) {
        // Ucięcie stringa w miejscu, gdzie znajduje się znak null
        _str = _str.substr(0, pos);
    }
}
////////////////////////////////////////////////////////////////////////////
bool CJsonConfigReaderWriter::readJsonFrameFromPC()
{
    auto frame = framesContainer->getFrameFromFront();
    bool isNewConfig = false;
    bool newMinSpeedVal, newMaxSpeedVal, newMaxAcc, newCarName;

    std::string jsonString( frame.begin(), frame.end() );
    Json json = Json::parse(jsonString);


    newMinSpeedVal = updateConfigValue<int>(json, "minSpeed", minSpeed );
    newMaxSpeedVal = updateConfigValue<int>(json, "maxSpeed", maxSpeed );
    newMaxAcc = updateConfigValue<int>(json, "maxAcc", maxAcc );
    newCarName = updateConfigValue<std::string>(json, "carName", carName );

    isNewConfig = newMinSpeedVal || newMaxSpeedVal || newMaxAcc || newCarName;

    if( isNewConfig )
    {
        createJson();
    }

    if( newMinSpeedVal )
    {
        infoSender->sendNewMinSpeedInfo(minSpeed);
        infoLcdManager->showMinSpeed(minSpeed, CInfoLcdManager::InfoValType::NEW_VAL );
    }
    if( newMaxSpeedVal )
    {
        infoSender->sendNewMaxSpeedInfo(maxSpeed);
        infoLcdManager->showMaxSpeed(maxSpeed, CInfoLcdManager::InfoValType::NEW_VAL );
    }
    if( newMaxAcc )
    {
        infoSender->sendNewMaxAccInfo(maxAcc);
        infoLcdManager->showMaxAcc(maxAcc, CInfoLcdManager::InfoValType::NEW_VAL );
    }
    if( newCarName )
    {
        infoSender->sendNewCarNameInfo(carName);
        infoLcdManager->showCarName(carName, CInfoLcdManager::InfoValType::NEW_VAL );
    }
//    if( json.find("minSpeed") != json.end() )
//    {
//        auto newMinSpeed = json.find("minSpeed")->get<int>();
//        if( newMinSpeed != minSpeed )
//        {
//            minSpeed = newMinSpeed;
//            infoSender->sendNewMinSpeedInfo( minSpeed );
//            isNewConfig = true;
//        }
//    }
//
//    if( json.find("maxSpeed") != json.end() )
//    {
//        auto newMaxSpeed = json.find("maxSpeed")->get<int>();
//        if( newMaxSpeed != maxSpeed )
//        {
//            maxSpeed = newMaxSpeed;
//            infoSender->sendNewMaxSpeedInfo( maxSpeed );
//            isNewConfig = true;
//        }
//    }
//
//    if( json.find("maxAcc") != json.end() )
//    {
//        auto newMaxAcc = json.find("maxAcc")->get<float>();
//        if( newMaxAcc != maxAcc )
//        {
//            maxAcc = newMaxAcc;
//            infoSender->sendNewMaxAccInfo( maxAcc );
//            isNewConfig = true;
//        }
//    }
//
//    if( json.find("carName") != json.end() )
//    {
//        auto newCarName = json.find("carName")->get<std::string>();
//        if( newCarName != carName )
//        {
//            carName = newCarName;
//            infoSender->sendNewCarNameInfo( carName );
//            isNewConfig = true;
//        }
//    }

//    if( isNewConfig )
//    {
//        createJson();
//    }
    return isNewConfig;
}

template <typename T>
bool CJsonConfigReaderWriter::updateConfigValue(Json& json, const std::string& key, T& currentValue )
{
    if( json.find(key) != json.end() )
    {
        T newValue = json.find(key)->get<T>();
        if (newValue != currentValue)
        {
            currentValue = newValue;
            return true;
        }
    }
    return false;
}

void CJsonConfigReaderWriter::processFramesFromPc()
{

    while( framesContainer->isFrameToProcessed() )
    {
        readJsonFrameFromPC();
    }

}

int CJsonConfigReaderWriter::getMinSpeed() const
{
    return minSpeed;
}
int CJsonConfigReaderWriter::getMaxSpeed() const
{
    return maxSpeed;
}
int CJsonConfigReaderWriter::getMaxAcc() const
{
    return maxAcc;
}
std::string CJsonConfigReaderWriter::getCarName() const
{
    return carName;
}
