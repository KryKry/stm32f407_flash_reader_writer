/*
 * Mcu2.h
 *
 *  Created on: May 21, 2024
 *      Author: kryst
 */

#ifndef INC_MCU2_H_
#define INC_MCU2_H_


#include "main.h"

#define SPI_FLASH_WRITE_TIMEOUT 5
#define SPI_FLASH_READ_TIMEOUT 5
#define SPI_FLASH_MIN_TIME_SPACE_MS_BETWEEN_FRAMES 1

extern SPI_HandleTypeDef hspi1;
//extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);

HAL_StatusTypeDef mcu_spi_flash_transmit_data(uint8_t* _data, size_t _size);
HAL_StatusTypeDef mcu_spi_flash_receive_data(uint8_t* _data, size_t _size);
void mcu_spi_flash_chip_select_pin_set();
void mcu_spi_flash_chip_select_pin_reset();
void mcu_delay_ms(uint32_t _timeMs);
void mcu_enable_usb_interrupt();
void mcu_disable_usb_interrupt();
void mcu_usb_send_frame(uint8_t* _data, uint16_t _len);

#endif /* INC_MCU2_H_ */
